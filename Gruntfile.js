'use strict';
module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        less: {
            development: {
                options: {
                    compress: true,
                    optimization: 2
                },
                files: {
                    "app/styles/styles.css": "app/less/index.less"
                }
            }
        },
        watch: {
            styles: {
                files: ['app/less/**/*.less'],
                tasks: ['less'],
                options: {
                    nospawn: true
                },
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        './app/styles/styles.css',
                        './app/**/*.js',
                        '!./app/**/*.test.js',
                        './app/**/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './app'
                }
            }
        },
        karma: {
            'unit': {
                'options': {
                    'autoWatch': false,
                    'browsers': ['PhantomJS'],
                    'configFile': './test/karma.conf.js',
                    'singleRun': true
                }
            }
        }
    });

    grunt.registerTask('test', ['karma']);

    grunt.registerTask('server', [
        'less',
        'browserSync',
        'watch'
    ]);

};
