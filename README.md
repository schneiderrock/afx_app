## Server starts on http://localhost:3000/

Two tasks are combined in one project.
* http://localhost:3000/#/ - Programming test
* http://localhost:3000/#/design - Design test


### Build the project:

```
npm install
bower install
```

### Local server and testing
```
grunt server
grunt test
```
