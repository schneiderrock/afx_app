'use strict';
angular.module('ifx')
.service('Api', ['$http', function($http){
	return {
		getImages: function() {
			return $http.jsonp('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tags=dogs');
		}
	}
}]);