angular.module('ifx', ['ngRoute'])
.config(function($routeProvider) {
        $routeProvider

            .when('/', {
                templateUrl : 'main/main.html',
                controller  : 'MainController',
                controllerAs: 'vm'
            })

            .when('/design', {
                templateUrl : 'pages/design.html',
            });
    });
