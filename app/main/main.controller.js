'use strict';

angular.module('ifx')
.controller('MainController', ['$scope', 'Api', function MainController($scope, Api) {
    var vm = this;
    vm.images = [];
    vm.storedImages = (localStorage["images"]) ? JSON.parse(localStorage["images"]) : [];

    Api.getImages().then(function(response) {
        vm.images = response.data.items;
    }, function(response) {
        console.log(response)
    });

    vm.checkImage = function(image) {
        var index = vm.storedImages.indexOf(image);
        if (index > -1) {
            vm.storedImages.splice(index, 1);
        } else {
            vm.storedImages.push(image);
        }

        localStorage["images"] = JSON.stringify(vm.storedImages);
    }

    vm.isSelected = function(image) {
        var index = vm.storedImages.indexOf(image);
        if (index > -1) {
            return true;
        } else {
            return false;
        }
    }

}]);
