describe('ifx', function() {

    beforeEach(function(){
        module('ifx');
    });

    describe("MainController", function () {
        var scope, vm, httpBackend;

        beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
            localStorage.clear();
            scope = $rootScope;
            httpBackend = $httpBackend;
            vm = $controller('MainController', {
                $scope: scope.$new()
            });
        }));

        it('should be defined', function(){
            expect(vm).toBeDefined();
            expect(vm.storedImages).toEqual([]);
        });

        it('should select image', function() {
            vm.checkImage('image1');
            expect(vm.storedImages).toEqual(['image1']);
            vm.checkImage('image2');
            vm.checkImage('image1');
            expect(vm.storedImages).toEqual(['image2']);
        });

        it('should check if image is selected', function(){
            vm.checkImage('image2');
            vm.checkImage('image1');
            expect(vm.isSelected('image1')).toBeTruthy();
            expect(vm.isSelected('image3')).toBeFalsy();
        });

        it('should get images from Flickr', function() {

            httpBackend.whenJSONP('http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=JSON_CALLBACK&tags=dogs').respond(200, {
                "title": "Recent Uploads tagged dogs",
                "link": "http://www.flickr.com/photos/tags/dogs/",
                "description": "",
                "modified": "2016-01-19T09:43:47Z",
                "generator": "http://www.flickr.com/",
                "items": [{
                    "title": "A walk through Pamber Forest",
                    "link": "http://www.flickr.com/photos/cameronpictures/23850769483/",
                    "media": {"m":"http://farm2.staticflickr.com/1495/23850769483_aa2efeb48e_m.jpg"},
                    "date_taken": "2016-01-16T15:23:13-08:00",
                    "description": " <p><a href=\"http://www.flickr.com/people/cameronpictures/\">SteeBoo<\/a> posted a photo:<\/p> <p><a href=\"http://www.flickr.com/photos/cameronpictures/23850769483/\" title=\"A walk through Pamber Forest\"><img src=\"http://farm2.staticflickr.com/1495/23850769483_aa2efeb48e_m.jpg\" width=\"160\" height=\"240\" alt=\"A walk through Pamber Forest\" /><\/a><\/p> ",
                    "published": "2016-01-19T09:43:47Z",
                    "author": "nobody@flickr.com (SteeBoo)",
                    "author_id": "9946019@N06",
                    "tags": "cold dogs forest countryside boots walk hats hampshire springer scarves coats wellies labradors joules spaniels pamber"
                }]
            });
            httpBackend.flush();
            expect(vm.images.length).toBe(1);
            expect(vm.images[0].media.m).toEqual('http://farm2.staticflickr.com/1495/23850769483_aa2efeb48e_m.jpg');

            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });
    });
});
